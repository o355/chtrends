# CenHud Outage Trends (chtrends) Version 3.1.1
# Copyright (C) 2021  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from CHData import CHData
chd = CHData("config.cfg")
delay = int(chd.config['COLLECTION']['delaytime'])
chd.getchdata(delay, True)