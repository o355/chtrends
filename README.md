# CenHud Outage Trends 3.1.1
Webpage/server/tracker combo for tracking Central Hudson's outage data over time.

# What's new with Version 3?
v3 of CenHud Outage Trends finally makes the codebase more object-oriented, with no spaghetti to boot.

Thanks to everything being much more object-oriented, it's much easier to adapt CenHud Outage Trends to your configuration needs, such as using different database providers, or tracking different power companies.

The web interface is now running on Bootstrap 5. There's minor improvements, including time localization and automatic refreshes.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

CenHud Outage Trends v3 spaghet-o-meter: **1/10**

CHTrends v3 is largely not spaghetti code. A lot of the work is done on the server-side in discrete classes using inheritance. There's a few places where some work could be done to improve line-efficiency (namely TrendsEngine), but otherwise, it's good.

v3.1/v3.1.1 reduces some of the spaghetti.

# Setting up CHTrends
CHTrends is a Flask-based app. It requires Python 3, and optionally some sort of WSGI interface to host the app.

Libraries needed for operation include:
* boto3 <-- Not required if you intend to use a JSON file for storage (read on for more info)
* requests
* json <-- Not required if you intend to use DynamoDB for storage (read on for more info)
* datetime
* flask_limiter
* flask

These can be installed by using the command `pip3 install boto3 requests json datetime flask_limiter flask`.

From CHTrends v3.1.1, a local JSON file is now used by default. You do NOT need a DynamoDB database. Instructions remain on how to set up JSON or DynamoDB.

If you are setting up via DynamoDB, you'll need to modify `CHData.py` and `TrendsEngine.py` to rely on DynamoDB (switch `JSONWrapper` with `DynamoDBWrapper` in the class statements - and import DynamoDBWrapper by using `from DynamoDBWrapper import DynamoDBWrapper`). Stay tuned for a better solution to this.

## JSON setup
This is the easier setup, where a local JSON file is used on your machine. First, make a JSON file (any filename can do, we'll use `data.json` for our purposes). The contents of this file should just be

```buildoutcfg
{}
```
So it's recognized properly. In config.cfg, you'll then want to put the path to the JSON file.

### Warning!
When running CHTrends on a web server, you should put the absolute path to the JSON file to avoid potential issues with relative directories.

## DynamoDB setup
If you want to use DynamoDB with CenHud Outage Trends, make sure you have an AWS Account with DynamoDB. With light traffic, you should remain within the free tier limits for DynamoDB.

Once you've got DynamoDB set up, make a table with this configuration:
* Name is up to you (defined in the configuration file)
* Primary key is timestamp
* No sort key
* Use 1 read/write capacity unit - this should work fine for 99% of configs. You may need to scale this up depending on your web server traffic.

Once you make this database, head into the IAM section of DynamoDB, making a set of access/primary keys that have full access to the database you just created. Keep node of these for the configuration setup.

## config.cfg setup
Depending on which setup method you used, you'll have different configuration values to fill out.

### DynamoDB
There are 4 keys you'll need to fill out in config.cfg - all in the AWS section.

Your access/primary keys to DynamoDB will go in the access_key and secret_key fields respectively.

The name of your database will go into the dbname field.

The region code of your DynamoDB database goes into the region field. For Ohio, this is `us-east-2`. For Virginia, this is `us-east-1`. For other regions, a quick Google search should get you what you need.

### JSON
There is 1 key you'll need to modify - under the JSON section. This is where you put where your JSON file is.

For any configuration type, additional modification is needed for the grab data script.

You'll also need to modify serve.py directly, so it knows where your configuration file is.

## Automatically grabbing data
The included `grabdata.py` script will automatically grab data. Set this on a cron timer for every 10 minutes, ensuring your working directory is set correctly.

This can be done by executing a simple shell script:
```buildoutcfg
cd /path/to/your/chtrends/install
python3 getdata.py
```

There are two configuration options under COLLECTION you will need to modify.

`collection` (in seconds) is used to tell the web frontend when to automatically refresh. This should reflect how often you are grabbing data. This is 600s by default, but you may need to change this for different power providers.

`delaytime` (in seconds) is used to tell grabdata.py how long to wait for new data to become available AND the web frontend when to automatically refresh. This is set to 15s by default.

# API Documentation
API Documentation can be found at https://chtrends.owenthe.dev/api. An OpenAPI v3 spec can be found in the `static/apispec.json` file.

# License
CenHud Outage Trends v3 continues to be licensed under the AGPL v3 license. 

Why continue with the AGPL v3? A few reasons:
* If you make a version of CHTrends that runs on a different provider, by requiring you to distribute the code, it allows others to inspect and reuse your code for power providers running on the same infrastructure.
* CHTrends is **not** a library. It is a full-stack web app. While does have components that could be integrated into a library, it's primary purpose is a full-stack web app.
* No separate codebases are needed for v2/v3 because of relicensing.

I understand that the complexity of the AGPL v3 may discourage people from cloning the project and modifying it. However, you only need to release your source code if you modify CHTrends and run it on the web.

You don't need to redistribute your modified code if you're just using it for your own personal use. Check the AGPL v3 license for all the details.

# Further customization
Because CHTrends v3 is very object-oriented, there's a few customizations you can do to the codebase.

## Changing DB providers
At the core of CHTrends, you need to provide a method that can properly do getting all data, putting, and deleting. If you'd rather use a JSON file (which is what v3.1 will rely on), or any other DB provider, making your own custom class with some Python knowledge is pretty easy.

## Changing power providers
Using a different power provider gets a bit more complicated. CHData is a standalone class that only inherits your DB provider - so you can rename methods as you wish. 

Depending on what system your power provider uses, you'll need to adapt the get methods. At the end of the day, you'll need to put in a new datapoint with a timestamp with the total customers out and total outages.

## Rate limiting
CHTrends 3.1.1 has rate limiting in place via the flask_limiter library. All configuration is done in serve.py. Working to improve this for future versions.