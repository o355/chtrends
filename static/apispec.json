{
    "openapi": "3.0.2",
    "info": {
        "title": "CenHud Outage Trends API",
        "version": "3.1.1",
        "description": "# CenHud Outage Trends API\nWelcome to the CenHud Outage Trends API! This API allows you to interface with the Central Hudson Outage Trends database (and Central Hudson's data in general) without having to worry about dealing with S3 data.\n\n# Rate Limits & Authentication\nNo authentication is needed to access the API. Rate limits are enforced by 360 requests/minute from v3.1.1 onward."
    },
    "servers": [
        {
            "url": "https://chtrends.owenthe.dev/api",
            "description": "Main CHTrends Web Server"
        }
    ],
    "paths": {
        "/getData": {
            "summary": "Get a specific types of data from the CenHud Trends database.",
            "description": "There are three types of data you can request.\n\nAll Data (solo) - All data from CHTrends going back 72 hours.\n\nLatest - The latest trend data from CHTrends.\n\nRecords - Records set in the last 72 hours.",
            "servers": [
                {
                    "url": "https://chtrends.owenthe.dev/api/v1",
                    "description": "Main CHTrends server"
                }
            ],
            "get": {
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/AllData"
                                }
                            }
                        },
                        "description": "The request completed successfully."
                    },
                    "400": {
                        "description": "You were missing the dataType query, or one of the datatypes requested is invalid."
                    },
                    "500": {
                        "description": "A deeper application error occurred when executing your request on the server side. If you encounter a 500, please get in touch with me (email is on the main owenthe.dev website)."
                    },
                    "429": {
                        "description": "You are being rate limited. Standard rate limit headers are included so you know when you can start making requests again."
                    }
                },
                "summary": "getData",
                "description": "Get specific types of data from the CenHud Outage Trends database. These data types are as follows:\n\n`alldata` - 72 hours worth of customers out/outage counts from the database, indexed by timestamp as a string.\n\n`latest` - The latest information from Central Hudson as passed into CenHud Outage Trends.\n\n`records` - Record high datapoints as logged by CenHud Outage Trends.\n\n`combined` - Combines all three datatypes above.\n\nDefine the datatypes you want to get in the dataTypes query, separated by a comma with no space."
            },
            "parameters": [
                {
                    "name": "dataTypes",
                    "description": "The datatypes you are requesting. This can be a list separated by a comma with no spaces of `alldata`, `latest`, or `records`. Optionally, you can pass `combined`, which is functionally the same as passing `alldata,latest,records`.",
                    "schema": {
                        "enum": [
                            "alldata",
                            "latest",
                            "records",
                            "combined"
                        ],
                        "type": "string"
                    },
                    "in": "query",
                    "required": true
                }
            ]
        },
        "/getTrends": {
            "get": {
                "servers": [
                    {
                        "url": "https://chtrends.owenthe.dev/api/v1",
                        "description": "Main CHTrends Server"
                    }
                ],
                "parameters": [
                    {
                        "name": "intervals",
                        "description": "The trend intervals you'd like to get. These are hours separated by a comma with no space (e.g. `1,3,6,12`). This can be any integer between 1 and 72 inclusive (1 and 71 inclusive if using centered average).\n\nv3.1.1 will enable the use of minutes divisible by 10 for the intervals via an optional parameter to indicate you're sending minutes.",
                        "schema": {
                            "type": "integer"
                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "useCentered",
                        "description": "Whether to use centered average data for trends rather than the raw data. Using the centered average can \"smooth\" out rough increases in the data.\n\nCentered averages are calculated using 3 points.\n\nThe server will assume you are passing this through as a boolean as string (true/false/True/False, in lowercase).",
                        "schema": {
                            "type": "boolean"
                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "useMinutes",
                        "description": "Whether you are using minutes in your interval query. If this query interval is not present, the assumption is made you are using hours in your interval query.\n\nWhen using minutes, your intervals must be divisible by 10. Bounds are 10-4320 (inclusive) for not using centered average, and 20-4310 (inclusive) for using centered average.",
                        "schema": {
                            "type": "boolean"
                        },
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/TrendsData"
                                }
                            }
                        },
                        "description": "The request completed successfully."
                    },
                    "400": {
                        "description": "You were missing one or both required query parameters, your query parameters had bad data types, or your intervals were out of bounds."
                    },
                    "429": {
                        "description": "You are being rate limited. Standard rate limit headers are included so you know when you can start making requests again."
                    },
                    "500": {
                        "description": "A deeper application error occurred when executing your request on the server side. If you encounter a 500, please get in touch with me (email is on the main owenthe.dev website)."
                    }
                },
                "summary": "getTrends",
                "description": "Get calculated trend data from CenHud Outage Trends. You can customize how you want to receive trend data depending on URL arguments/form data.\n\nThere's many different types of trends that can be returned. The example response will list out all the potential responses."
            }
        }
    },
    "components": {
        "schemas": {
            "AllData": {
                "title": "Root Type for AllData",
                "description": "A response associated with running all data.",
                "type": "object",
                "properties": {
                    "version": {
                        "description": "The API version being used.",
                        "type": "string"
                    },
                    "code": {
                        "format": "int32",
                        "type": "integer"
                    },
                    "alldata": {
                        "description": "The dictionary returned if alldata (or combined) was part of the datatype query, representing all the data returned from the database.",
                        "type": "object",
                        "properties": {
                            "1615000000": {
                                "description": "A dictionary representing a point in time, as denoted by the timestamp as the key.",
                                "type": "object",
                                "properties": {
                                    "customers": {
                                        "format": "int32",
                                        "description": "The number of customers out of power at this point in time.",
                                        "type": "integer"
                                    },
                                    "outages": {
                                        "format": "int32",
                                        "description": "The number of individual power outages at this point in time.",
                                        "type": "integer"
                                    }
                                }
                            }
                        }
                    },
                    "latest": {
                        "description": "The dictionary returned if latest (or combined) was part of the datatypes query. Represents the latest data retrieved from Central Hudson.",
                        "type": "object",
                        "properties": {
                            "timestamp": {
                                "format": "int32",
                                "description": "The UNIX timestamp of when the latest data was collected.",
                                "type": "integer"
                            },
                            "formattedtime": {
                                "description": "A formatted time string of when the latest data was collected. Is in Eastern Time, and should be used as a fallback for user-facing programs.",
                                "type": "string"
                            },
                            "customers": {
                                "format": "int32",
                                "description": "The number of customers currently out of power.",
                                "type": "integer"
                            },
                            "customers_formatted": {
                                "description": "The number of customers currently out of power, but formatted for user-facing programs.",
                                "type": "string"
                            },
                            "outages": {
                                "format": "int32",
                                "description": "The number of individual power outages ongoing.",
                                "type": "integer"
                            },
                            "outages_formatted": {
                                "description": "The number of individual power outages ongoing, but formatted for user-facing programs.",
                                "type": "string"
                            }
                        }
                    },
                    "records": {
                        "description": "The dictionary returned if records (or combined) was part of the datatypes query. Represents the records found in the database in the last 72 hours.",
                        "type": "object",
                        "properties": {
                            "customers": {
                                "description": "The dictionary object for the customers out record.",
                                "type": "object",
                                "properties": {
                                    "number": {
                                        "format": "int32",
                                        "description": "The number of customers that were out of power when the record occurred.",
                                        "type": "integer"
                                    },
                                    "number_formatted": {
                                        "description": "The number of customers that were out of power when the record occurred, but formatted for user-facing programs.",
                                        "type": "string"
                                    },
                                    "timestamp": {
                                        "format": "int32",
                                        "description": "The UNIX timestamp of when the record number of customers out of power occurred.",
                                        "type": "integer"
                                    },
                                    "formattedtime": {
                                        "description": "A formatted time string of when the record number of customers out of power occurred. Is in Eastern Time, and should be used as a fallback for user-facing programs.",
                                        "type": "string"
                                    },
                                    "newrecord": {
                                        "description": "Whether this record occurred on the most recent data retrieval from Central Hudson.",
                                        "type": "boolean"
                                    }
                                }
                            },
                            "outages": {
                                "description": "The dictionary object for the individual power outages record.",
                                "type": "object",
                                "properties": {
                                    "number": {
                                        "format": "int32",
                                        "description": "The number of individual power outages that were ongoing when the record occurred.",
                                        "type": "integer"
                                    },
                                    "number_formatted": {
                                        "description": "The number of individual power outages that were ongoing when the record occurred, but formatted for user-facing programs.",
                                        "type": "string"
                                    },
                                    "timestamp": {
                                        "format": "int32",
                                        "description": "The UNIX timestamp for when the record number of individual power outages occurred.",
                                        "type": "integer"
                                    },
                                    "formattedtime": {
                                        "description": "A formatted time string of when the record number of individual power outages occurred. Is in Eastern Time, and should be used as a fallback for user-facing programs.",
                                        "type": "string"
                                    },
                                    "newrecord": {
                                        "description": "Whether this record occurred on the most recent data retrieval from Central Hudson.",
                                        "type": "boolean"
                                    }
                                }
                            }
                        }
                    }
                },
                "example": {
                    "version": "v3.1.0",
                    "code": 0,
                    "alldata": {
                        "1615000000": {
                            "customers": 75,
                            "outages": 25
                        }
                    },
                    "latest": {
                        "timestamp": 1615001200,
                        "formattedtime": "May 23, 2021 at 9:00 PM",
                        "customers": 2500,
                        "customers_formatted": "2,500",
                        "outages": 1000,
                        "outages_formatted": "1,000"
                    },
                    "records": {
                        "customers": {
                            "number": 15000,
                            "number_formatted": "15,000",
                            "timestamp": 1615001200,
                            "formattedtime": "May 23, 2021 at 9:00 PM",
                            "newrecord": true
                        },
                        "outages": {
                            "number": 1500,
                            "number_formatted": "1,500",
                            "timestamp": 161500000,
                            "formattedtime": "May 23, 2021 at 8:40 PM",
                            "newrecord": false
                        }
                    }
                }
            },
            "TrendsData": {
                "title": "Root Type for TrendsData",
                "description": "Data returned from a trends object.",
                "type": "object",
                "properties": {
                    "version": {
                        "description": "The version of the CHTrends API being used.",
                        "type": "string"
                    },
                    "code": {
                        "format": "int32",
                        "description": "The response code returned by the API.",
                        "type": "integer"
                    },
                    "customers": {
                        "description": "The array of trend data for customers out.",
                        "type": "array",
                        "items": {
                            "description": "The entries for trend data. This will be in the same order that you requested (e.g. you request `1,3,6,12`, index 0 will contain 1 hour trend data, index 1 will contain 3 hour trend data, etc.)",
                            "type": "object",
                            "properties": {
                                "duration": {
                                    "description": "A string representing the trend datapoint being used for this entry.",
                                    "type": "string"
                                },
                                "change_percent": {
                                    "description": "The percent change of customers out from the trend datapoint to the current datapoint as a percent. This is not present when data is not available, and \"infinity\" when the trend datapoint is 0 customers.",
                                    "type": "integer"
                                },
                                "change_numeric": {
                                    "description": "The change in number of customers out from the trend datapoint to the current datapoint as a number. This is not present when data is not available.",
                                    "type": "string"
                                },
                                "change_direction": {
                                    "description": "A string representing the direction of change from the trend datapoint to the current datapoint. This can be Up, Down, or No change.",
                                    "type": "string"
                                },
                                "change_english": {
                                    "description": "An English string representing the trend, combining the previous data. Use this in conjunction with change_direction for user-facing programs.",
                                    "type": "string"
                                }
                            }
                        }
                    },
                    "outages": {
                        "description": "The array of trend data for unique power outages.",
                        "type": "array",
                        "items": {
                            "description": "The entries for trend data. This will be in the same order that you requested (e.g. you request `1,3,6,12`, index 0 will contain 1 hour trend data, index 1 will contain 3 hour trend data, etc.)",
                            "type": "object",
                            "properties": {
                                "duration": {
                                    "description": "A string representing the trend datapoint being used for this entry.",
                                    "type": "string"
                                },
                                "change_percent": {
                                    "description": "The percent change from the trend datapoint to the current datapoint as a percent. This is not present when data is not available, and \"infinity\" when the trend datapoint is 0 customers.",
                                    "type": "integer"
                                },
                                "change_numeric": {
                                    "description": "The change in number of customers out from the trend datapoint to the current datapoint as a number. This is not present when data is not available.",
                                    "type": "string"
                                },
                                "change_direction": {
                                    "description": "A string representing the direction of change from the trend datapoint to the current datapoint. This can be Up, Down, or No change.",
                                    "type": "string"
                                },
                                "change_english": {
                                    "description": "An English string representing the trend, combining the previous data. Use this in conjunction with change_direction for user-facing programs.",
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "example": {
                    "version": "v3.1.0",
                    "code": 0,
                    "customers": [
                        {
                            "duration": "1 hour",
                            "change_direction": "N/A",
                            "change_english": "Data unavailable in the last hour"
                        },
                        {
                            "duration": "3 hours",
                            "change_percent": "infinity",
                            "change_numeric": 15,
                            "change_direction": "Up",
                            "change_english": "Infinity% (+15 customers) in the last 3 hours"
                        },
                        {
                            "duration": "6 hours",
                            "change_percent": -75,
                            "change_numeric": -25,
                            "change_direction": "Down",
                            "change_english": "75% (-25 customers) in the last 6 hours"
                        },
                        {
                            "duration": "12 hours",
                            "change_percent": 75,
                            "change_numeric": 25,
                            "change_direction": "Up",
                            "change_english": "75% (+25 customers) in the last 12 hours"
                        },
                        {
                            "duration": "24 hours",
                            "change_percent": 0,
                            "change_numeric": 0,
                            "change_direction": "No change",
                            "change_english": "in the last 24 hours"
                        }
                    ],
                    "outages": [
                        {
                            "duration": "1 hour",
                            "change_direction": "N/A",
                            "change_english": "Data unavailable in the last hour"
                        },
                        {
                            "duration": "3 hours",
                            "change_percent": "infinity",
                            "change_numeric": 15,
                            "change_direction": "Up",
                            "change_english": "Infinity% (+15 outages) in the last 3 hours"
                        },
                        {
                            "duration": "6 hours",
                            "change_percent": -75,
                            "change_numeric": -25,
                            "change_direction": "Down",
                            "change_english": "75% (-25 outages) in the last 6 hours"
                        },
                        {
                            "duration": "12 hours",
                            "change_percent": 75,
                            "change_numeric": 25,
                            "change_direction": "Up",
                            "change_english": "75% (+25 outages) in the last 12 hours"
                        },
                        {
                            "duration": "24 hours",
                            "change_percent": 0,
                            "change_numeric": 0,
                            "change_direction": "No change",
                            "change_english": "in the last 24 hours"
                        }
                    ]
                }
            }
        }
    }
}