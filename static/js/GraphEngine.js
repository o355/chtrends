/* CenHud Outage Trends (chtrends) Version 3.0.0
  Copyright (C) 2021  Owen (owenthe.dev)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class GraphEngine {
    constructor(graphelem, data, graphstate) {
        this.graphelem = graphelem
        if (graphstate == undefined || graphstate == null) {
            this.graphstate = "normal"
        } else {
            this.graphstate = graphstate
        }
        this.data = data
    }

    rendergraph() {
        var localdata = new google.visualization.DataTable();
        var key;
        if (this.graphstate == "normal") {
            localdata.addColumn('date', 'Time')
            localdata.addColumn('number', 'Customers Out')
            localdata.addColumn('number', 'Outages')
    
            for (key in this.data) {
                localdata.addRow([new Date(key * 1000), this.data[key]['customers'], this.data[key]['outages']])
            }
        } else if (this.graphstate == "co") {
            localdata.addColumn('date', 'Time')
            localdata.addColumn('number', 'Customers Out')
    
            for (key in this.data) {
                localdata.addRow([new Date(key * 1000), this.data[key]['customers']])
            }
        } else if (this.graphstate == "o") {
            localdata.addColumn('date', 'Time')
            localdata.addColumn('number', 'Outages')
    
            for (key in this.data) {
                localdata.addRow([new Date(key * 1000), this.data[key]['outages']])
            }
        }

        if (this.graphstate == "o") {
            var options = {
                hAxis: {
                    title: 'Time',
                    format: 'M/d, h a',
                },
                vAxis: {
                    format: 'decimal'
                },
                series: {
                    0: {
                        color: "#db4437"
                    }
                }
            };
        } else {
            var options = {
                hAxis: {
                    title: 'Time',
                    format: 'M/d, h a',
                },
                vAxis: {
                    format: 'decimal'
                }
            };
        }
        if (window.innerWidth <= 992) {
            options.legend = {}
            options.legend.position = "none"
        }

        var formatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        formatter.format(localdata, 0);
        var chart = new google.charts.Line(document.getElementById(this.graphelem));
        chart.draw(localdata, google.charts.Line.convertOptions(options));
    }

    graphtypechange(type) {
        this.graphstate = type
        this.rendergraph()
    }

}