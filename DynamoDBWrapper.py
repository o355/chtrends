# CenHud Outage Trends (chtrends) Version 3.1.1
# Copyright (C) 2021  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import configparser
import boto3


class DynamoDBWrapper():
    """
    DynamoDBWrapper is a low-level class to directly interface with DynamoDB. For higher-level methods that include
    automatic purge deletion, getting the latest key, please use the CHData class.
    """

    def __init__(self, configfile):
        """
        Construct a new DynamoDBWrapper object.
        :param configfile: A configuration file to read from to access the AWS keys/region name/DB name.
        """
        self.config = configparser.ConfigParser()
        self.config.read(configfile)

        self.session = boto3.Session(aws_access_key_id=self.config['AWS']['access_key'], aws_secret_access_key=self.config['AWS']['secret_key'], region_name=self.config['AWS']['region'])
        self.db = self.session.resource("dynamodb")
        self.table = self.db.Table(self.config['AWS']['dbname'])

    def getall(self):
        """
        Does a whole table scan, returning all data that CHTrends knows of in the DB.
        :return: A Dictionary of format: timestamp as string for the key, customers/outages for each timestamp key as integers.
        """
        scan = self.table.scan()
        returnobj = {}

        for i in scan['Items']:
            returnobj[str(i['timestamp'])] = {"customers": int(i['customers']), "outages": int(i['outages'])}

        sorted_returnobj = {}
        for key in sorted(returnobj):
            sorted_returnobj[key] = returnobj[key]

        return sorted_returnobj

    def put(self, timestamp, outages, customers):
        """
        Puts new data in the database.
        :param timestamp: The UNIX timestamp to associate this key with. Must be convertible to an integer.
        :param outages: The number of outages that occurred at this timestamp. Must be convertible to an integer.
        :param customers: THe number of customers out that occurred at this timestamp. Must be convertible to an integer.
        :return: TypeError on bad conversion, AWS error if boto3 throws such an error, otherwise nothing if successful.
        """
        try:
            int(timestamp)
            int(outages)
            int(customers)
        except TypeError:
            raise TypeError("Timestamp, outages, and/or customers failed conversion to an integer.")

        self.table.put_item(
            Item={
                'timestamp': int(timestamp),
                'outages': int(outages),
                'customers': int(customers),
            }
        )

    def delete(self, timestamp):
        """
        Deletes a data point from the database. This will NOT do retroactive deletion - please use the CHData class to handle this.
        :param timestamp: The timestamp of the data point to delete. Must be convertible to an integer.
        :return: TypeError on bad conversion, AWS error if boto3 throws such an error, otherwise nothing if successful.
        """
        try:
            int(timestamp)
        except TypeError:
            raise TypeError("Timestamp failed conversion to an integer.")

        self.table.delete_item(
            Key={
                'timestamp': int(timestamp)
            }
        )