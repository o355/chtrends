# CenHud Outage Trends (chtrends) Version 3.1.1
# Copyright (C) 2021  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from flask import Flask, request, jsonify, render_template
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from TrendsEngine import TrendsEngine

app = Flask(__name__)

limiter = Limiter(
    app,
    key_func=get_remote_address,
    headers_enabled=True,
    strategy="fixed-window-elastic-expiry",
    default_limits=["360 per minute"]
)

configfile = "config.cfg"


@app.route("/", methods=['GET'])
@limiter.exempt
def main_route():
    te = TrendsEngine(configfile)
    currentdata = te.getlatest()
    trends_1_customers = te.gettrends([1], "customers", False)
    trends_2_customers = te.gettrends([3, 6, 12], "customers", False)
    trends_1_outages = te.gettrends([1], "outages", False)
    trends_2_outages = te.gettrends([3, 6, 12], "outages", False)
    recorddata = te.getrecords()
    alldata = te.data
    lastupdate_ts = int(te.latestkey)
    custrecord_ts = recorddata['customers']['timestamp']
    outagerecord_ts = recorddata['outages']['timestamp']
    collectiontime = int(te.config['COLLECTION']['collectionseconds'])
    delaytime = int(te.config['COLLECTION']['delaytime'])

    return render_template("page.html",
                           currentdata=currentdata,
                           t1customers=trends_1_customers,
                           t2customers=trends_2_customers,
                           t1outages=trends_1_outages,
                           t2outages=trends_2_outages,
                           recorddata=recorddata,
                           alldata=alldata,
                           lastupdate_ts=lastupdate_ts,
                           custrecord_ts=custrecord_ts,
                           outagerecord_ts=outagerecord_ts,
                           delaytime=delaytime,
                           collectiontime=collectiontime)


@app.route("/api/", methods=['GET'])
@limiter.exempt
def api_homepage():
    return render_template("api.html")


@app.route("/api", methods=['GET'])
@limiter.exempt
def call_api_hompeage():
    api_homepage()


@app.route("/api/v1/getData", methods=['GET'])
def getdata():
    datatypes = request.values.get("dataTypes", None)
    if datatypes is None:
        response = {"message": "Your dataTypes query parameter was not present."}
        response = jsonify(response)
        return response, 400

    datatypes_split = datatypes.split(",")
    reference_types = ["alldata", "latest", "records", "combined"]
    for datatype in datatypes_split:
        if datatype not in reference_types:
            response = {"message": "One of your dataTypes was invalid (%s)." % datatype}
            response = jsonify(response)
            return response, 400

    response = {"apiversion": "v3.1.1", "code": 0}
    te = TrendsEngine(configfile)

    if "alldata" in datatypes_split or "combined" in datatypes_split:
        response['alldata'] = te.getall()

    if "latest" in datatypes_split or "combined" in datatypes_split:
        response['latest'] = te.getlatest()

    if "records" in datatypes_split or "combined" in datatypes_split:
        response['records'] = te.getrecords()

    response = jsonify(response)
    # Define three types of data - all data (solo), latest, records
    return response, 200

@app.route("/api/v1/getTrends", methods=['GET'])
def gettrends():
    intervals = request.values.get("intervals", None)
    usecentered = request.values.get("useCentered", None)
    useminutes = request.values.get("useMinutes", "false")

    if intervals is None:
        response = {"message": "Your intervals query parameter was not present."}
        response = jsonify(response)
        return response, 400

    if usecentered is None:
        response = {"message": "Your usecentered query parameter was not present."}
        response = jsonify(response)
        return response, 400

    usecentered_reference = ["true", "True", "false", "False"]
    if usecentered not in usecentered_reference:
        response = {"message": "Your usecentered query must be one of true, True, false, or False."}
        response = jsonify(response)
        return response, 400

    if useminutes not in usecentered_reference:
        response = {"message": "Your useminutes query must be one of true, True, false, or False."}
        response = jsonify(response)
        return response, 400

    if usecentered == "true" or usecentered == "True":
        usecentered = True
    elif usecentered == "false" or usecentered == "False":
        usecentered = False

    if useminutes == "true" or useminutes == "True":
        useminutes = True
    elif useminutes == "false" or useminutes == "False":
        useminutes = False


    intervals_split = intervals.split(",")
    for interval in intervals_split:
        try:
            test = int(interval)
            if useminutes:
                if test % 10 != 0:
                    response = {
                        "message": "One of your intervals was not divisible by 10 (%s). You must have an interval divisible by 10 to use minutes." % interval}
                    response = jsonify(response)
                    return response, 400


            if usecentered:
                if useminutes:
                    if test < 20 or test > 4310:
                        response = {
                            "message": "One of your intervals was out of bounds (%s). Acceptable bounds are 20-4310 (inclusive & divisible by 10) for using centered average & minutes as intervals." % interval}
                        response = jsonify(response)
                        return response, 400
                else:
                    if test < 1 or test > 71:
                        response = {"message": "One of your intervals was out of bounds (%s). Acceptable bounds are 1-71 (inclusive) for using centered average." % interval}
                        response = jsonify(response)
                        return response, 400
            else:
                if useminutes:
                    if test < 10 or test > 4320:
                        response = {
                            "message": "One of your intervals was out of bounds (%s). Acceptable bounds are 10-4320 (inclusive & divisible by 10) for using minutes as intervals." % interval}
                        response = jsonify(response)
                        return response, 400
                else:
                    if test < 1 or test > 72:
                        response = {
                            "message": "One of your intervals was out of bounds (%s). Acceptable bounds are 1-72 (inclusive)." % interval}
                        response = jsonify(response)
                        return response, 400
        except ValueError:
            response = {"message": "One of your intervals couldn't be converted to a number (%s)." % interval}
            response = jsonify(response)
            return response, 400

    intervals_converted = []
    for interval in intervals_split:
        intervals_converted.append(int(interval))

    response = {"version": "v3.1.1", "code": 0}
    te = TrendsEngine(configfile)
    response['customers'] = te.gettrends(intervals_converted, "customers", usecentered, useminutes)
    response['outages'] = te.gettrends(intervals_converted, "outages", usecentered, useminutes)

    response = jsonify(response)
    return response, 200


if __name__ == "__main__":
    app.run()