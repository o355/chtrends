import requests
import time
import json
from JSONWrapper import JSONWrapper

# CenHud Outage Trends (chtrends) Version 3.1.1
# Copyright (C) 2021  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class CHData(JSONWrapper):
    """
    CHData extends the DynamoDBWrapper class, providing specific methods to more directly interface with the DB.
    """

    def __init__(self, configfile):
        """
        Creates a new CHData class.
        :param configfile: The config file to read from, including keys about purge times/URL access.
        """
        super().__init__(configfile)

        self.headers_chtrendsbot = {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded;charset=ISO-8859-1',
            'DNT': '1',
            'User-Agent': 'chtrends/3.0.0'
        }

    def __grabchdata(self, dictionary):
        site = requests.get(self.config['COLLECTION']['base_url'] + "/" + dictionary + "/data.json")
        data = json.loads(site.text)
        # Test if we got data - this will raise KeyError if it failed.
        test_customers = data['summaryFileData']['total_outages']
        return data

    def getchdata(self, waittime, autoprune=True):
        """
        Gets new outage data from Central Hudson.
        :param waittime: The amount of time to wait (in seconds) before capturing data. If you are running this on a cronjob that executes at :00, put this to about 15-25 seconds.
        :param autoprune: Whether to automatically prune data from the database. Set this to False if you would like to prune using different settings than the default ones.
        :return:
        """
        workingts = int(time.time()) // 600 * 600
        # Wait as specified
        time.sleep(waittime)
        # Get dictionary to use
        site = requests.get(self.config['COLLECTION']['base_url'] + "/metadata.json", headers=self.headers_chtrendsbot)
        data = json.loads(site.text)
        dicttouse = data['directory']

        # Then actually request the data
        try:
            data = self.__grabchdata(dicttouse)
        except KeyError:
            # Wait again
            time.sleep(waittime)
            try:
                data = self.__grabchdata(dicttouse)
            except:
                raise RuntimeError("Failed to grab data from Central Hudson after waiting.")

        # Then parse the data!
        outages = data['summaryFileData']['total_outages']
        customers = data['summaryFileData']['total_cust_a']['val']

        self.put(workingts, outages, customers)
        if autoprune:
            self.prunedb()

    def prunedb(self, usedblatest=False, retroactive=True):
        """
        Prunes the database of old data, using the retention period specified in the config file. Depending on your configuration settings, you will have different O() runtimes.
        Default - O(N). 1 scan query, 1 put query.
        UseDBLatest False, Retroactive False - 0(1). 0 scan queries, 1 put query.
        UseDBLatest True, Retroactive True - O(2N). 1 scan queries (looped through twice), 1 put query.
        :param usedblatest: Whether to use the latest data in the database as the starting prune point, or use the nearest rounded down UNIX time within 10 minutes.
        By default, this is False. This is an optional parameter.
        :param retroactive: Whether to retroactively delete old data in the database. If disabled, only point-in-time deletion will be done.
        :return: Nothing if successful.
        """
        latestkey = -1
        tablescan = None
        if usedblatest:
            tablescan = self.getall()
            # Because getall sorts by keys in reverse, the latest key will ALWAYS be at indice 0.
            latestkey = int(list(tablescan.keys())[-1])
        else:
            latestkey = int(time.time() // 600 * 600)

        if retroactive:
            if tablescan is None:
                tablescan = self.getall()

            for key in tablescan.keys():
                if int(key) + int(self.config['COLLECTION']['prunetime']) < latestkey:
                    self.delete(key)
        else:
            self.delete(latestkey - int(self.config['COLLECTION']['prunetime']))




