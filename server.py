# CenHud Outage Trends (chtrends) Version 2.0.4
# Copyright (C) 2020  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import Flask, request, jsonify, render_template, Response
import boto3
import datetime
from dateutil import tz
# v2.0.4 added a call service via Twilio. You can remove the inboundVoice method, and any lines below here up to...
from twilio.rest import Client
import twilio
from twilio.twiml.messaging_response import MessagingResponse
from twilio.twiml.voice_response import VoiceResponse, Say

client = Client("", "")
# ...here safely.
 
access_key_aws = "YOUR_ACCESS_KEY_HERE"
secret_key_aws = "YOUR_SECRET_KEY_HERE"

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name='us-east-2')
db = session.resource('dynamodb')
table = db.Table('PowerOutages_Unix')

app = Flask(__name__)


@app.route("/", methods=['GET'])
def main_route():
    tzdata = tz.gettz('America/New_York')
    year = str(datetime.datetime.now().year)
    month = str(datetime.datetime.now().month)
    day = str(datetime.datetime.now().day)
    hour = str(datetime.datetime.now().hour)
    minute = str(datetime.datetime.now().minute)
    second = "00"
    timezone = datetime.datetime.now(tz=tzdata).tzname()

    timezone_offset = "-0500"
    if timezone == "EDT":
        timezone_offset = "-0400"

    trend_time = datetime.datetime.now(tz=tzdata)
    trend_time_1h = trend_time + datetime.timedelta(hours=-1)
    trend_time_3h = trend_time + datetime.timedelta(hours=-3)
    trend_time_6h = trend_time + datetime.timedelta(hours=-6)
    trend_time_12h = trend_time + datetime.timedelta(hours=-12)

    trend1_year = str(trend_time_1h.year).zfill(2)
    trend1_month = str(trend_time_1h.month).zfill(2)
    trend1_day = str(trend_time_1h.day).zfill(2)
    trend1_hour = str(trend_time_1h.hour).zfill(2)
    trend1_minute = str(trend_time_1h.minute).zfill(2)
    trend1_second = "00"
    trend1_timezone = trend_time_1h.tzname()
    timezone_offset_trend1 = "-0500"
    if trend1_timezone == "EDT":
        timezone_offset_trend1 = "-0400"

    trend3_year = str(trend_time_3h.year).zfill(2)
    trend3_month = str(trend_time_3h.month).zfill(2)
    trend3_day = str(trend_time_3h.day).zfill(2)
    trend3_hour = str(trend_time_3h.hour).zfill(2)
    trend3_minute = str(trend_time_3h.minute).zfill(2)
    trend3_second = "00"
    trend3_timezone = trend_time_3h.tzname()
    timezone_offset_trend3 = "-0500"
    if trend3_timezone == "EDT":
        timezone_offset_trend3 = "-0400"

    trend6_year = str(trend_time_6h.year).zfill(2)
    trend6_month = str(trend_time_6h.month).zfill(2)
    trend6_day = str(trend_time_6h.day).zfill(2)
    trend6_hour = str(trend_time_6h.hour).zfill(2)
    trend6_minute = str(trend_time_6h.minute).zfill(2)
    trend6_second = "00"
    trend6_timezone = trend_time_6h.tzname()
    timezone_offset_trend6 = "-0500"
    if trend6_timezone == "EDT":
        timezone_offset_trend6 = "-0400"

    trend12_year = str(trend_time_12h.year).zfill(2)
    trend12_month = str(trend_time_12h.month).zfill(2)
    trend12_day = str(trend_time_12h.day).zfill(2)
    trend12_hour = str(trend_time_12h.hour).zfill(2)
    trend12_minute = str(trend_time_12h.minute).zfill(2)
    trend12_second = "00"
    trend12_timezone = trend_time_12h.tzname()
    timezone_offset_trend12 = "-0500"
    if trend12_timezone == "EDT":
        timezone_offset_trend12 = "-0400"

    year = year.zfill(2)
    month = month.zfill(2)
    day = day.zfill(2)
    hour = hour.zfill(2)
    minute = minute.zfill(2)
    minute = minute[0] + "0"

    trend1_minute = trend1_minute[0] + "0"
    trend3_minute = trend3_minute[0] + "0"
    trend6_minute = trend6_minute[0] + "0"
    trend12_minute = trend12_minute[0] + "0"

    formatted_date = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + " " + timezone_offset
    trend1_date = str(trend1_year) + "-" + str(trend1_month) + "-" + str(trend1_day) + " " + str(trend1_hour) + ":" + str(trend1_minute) + ":" + str(trend1_second) + " " + timezone_offset_trend1
    trend3_date = str(trend3_year) + "-" + str(trend3_month) + "-" + str(trend3_day) + " " + str(
        trend3_hour) + ":" + str(trend3_minute) + ":" + str(trend3_second) + " " + timezone_offset_trend3
    trend6_date = str(trend6_year) + "-" + str(trend6_month) + "-" + str(trend6_day) + " " + str(
        trend6_hour) + ":" + str(trend6_minute) + ":" + str(trend6_second) + " " + timezone_offset_trend6
    trend12_date = str(trend12_year) + "-" + str(trend12_month) + "-" + str(trend12_day) + " " + str(
        trend12_hour) + ":" + str(trend12_minute) + ":" + str(trend12_second) + " " + timezone_offset_trend12

    formatted_date_timestamp = datetime.datetime.strptime(formatted_date, "%Y-%m-%d %H:%M:%S %z")
    trend1_date_timestamp = datetime.datetime.strptime(trend1_date, "%Y-%m-%d %H:%M:%S %z")
    trend3_date_timestamp = datetime.datetime.strptime(trend3_date, "%Y-%m-%d %H:%M:%S %z")
    trend6_date_timestamp = datetime.datetime.strptime(trend6_date, "%Y-%m-%d %H:%M:%S %z")
    trend12_date_timestamp = datetime.datetime.strptime(trend12_date, "%Y-%m-%d %H:%M:%S %z")

    response = table.scan()
    fulldata = {}
    for i in response['Items']:
        timestamp = int(i['timestamp'])
        outagesthing = int(i['outages'])
        customersthing = int(i['customers'])
        fulldata[timestamp] = {
            "outages": outagesthing,
            "customers": customersthing
        }

    record_outages = -1
    record_customers = -1
    for i in response['Items']:
        if int(i['timestamp']) == int(formatted_date_timestamp.timestamp()):
            outages = int(i['outages'])
            customers = int(i['customers'])
            last_updated_time = formatted_date_timestamp.strftime("%B %-d at %l:%M %p")

        if int(i['outages']) > record_outages:
            record_outages = int(i['outages'])
            record_outages_time = datetime.datetime.fromtimestamp(int(i['timestamp'])).strftime("%B %-d at %l:%M %p")

        if int(i['customers']) > record_customers:
            record_customers = int(i['customers'])
            record_customers_time = datetime.datetime.fromtimestamp(int(i['timestamp'])).strftime("%B %-d at %l:%M %p")

        if int(i['timestamp']) == int(trend1_date_timestamp.timestamp()):
            trend1_outages = int(i['outages'])
            trend1_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend3_date_timestamp.timestamp()):
            trend3_outages = int(i['outages'])
            trend3_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend6_date_timestamp.timestamp()):
            trend6_outages = int(i['outages'])
            trend6_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend12_date_timestamp.timestamp()):
            trend12_outages = int(i['outages'])
            trend12_customers = int(i['customers'])


    try:
        test_customers = customers
    except Exception:
        last_updated_time = formatted_date_timestamp + datetime.timedelta(minutes=-10)
        last_updated_time_key = int(last_updated_time.timestamp())
        last_updated_time = last_updated_time.strftime('%B %-d at %l:%M %p')
        customers = fulldata[int(last_updated_time_key)]['customers']
        outages = fulldata[int(last_updated_time_key)]['outages']

    try:
        trend1_customers_diff = customers - trend1_customers
        if trend1_customers_diff >= 0:
            trend1_customers_diff = "+" + str("{:,}".format(trend1_customers_diff))
        else:
            trend1_customers_diff = str("{:,}".format(trend1_customers_diff))

        try:
            trend1_customers_percent = ((customers - trend1_customers) / trend1_customers) * 100
            trend1_customers_percent = round(trend1_customers_percent, 2)
            if trend1_customers_percent < 0:
                trend1_customers = "Down"
                trend1_customers_percent = str(
                    "{:,}".format(abs(trend1_customers_percent))) + "% (" + trend1_customers_diff + " customers) "
            elif trend1_customers_percent == 0:
                trend1_customers = "No change"
                trend1_customers_percent = ""
            else:
                trend1_customers = "Up"
                trend1_customers_percent = str(
                    "{:,}".format(trend1_customers_percent)) + "% (" + trend1_customers_diff + " customers) "

        except ZeroDivisionError:
            if trend1_customers_diff == "+0":
                trend1_customers = "No change"
                trend1_customers_percent = ""
            else:
                trend1_customers = "Up"
                trend1_customers_percent = "Infinity% (" + trend1_customers_diff + " customers) "
    except UnboundLocalError:
        trend1_customers = "N/A"
        trend1_customers_percent = "Data unavailable "

    try:
        trend3_customers_diff = customers - trend3_customers
        if trend3_customers_diff >= 0:
            trend3_customers_diff = "+" + str("{:,}".format(trend3_customers_diff))
        else:
            trend3_customers_diff = str("{:,}".format(trend3_customers_diff))

        try:
            trend3_customers_percent = ((customers - trend3_customers) / trend3_customers) * 100
            trend3_customers_percent = round(trend3_customers_percent, 2)
            if trend3_customers_percent < 0:
                trend3_customers = "Down"
                trend3_customers_percent = str(
                    "{:,}".format(abs(trend3_customers_percent))) + "% (" + trend3_customers_diff + " customers) "
            elif trend3_customers_percent == 0:
                trend3_customers = "No change"
                trend3_customers_percent = ""
            else:
                trend3_customers = "Up"
                trend3_customers_percent = str(
                    "{:,}".format(trend3_customers_percent)) + "% (" + trend3_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend3_customers_diff == "+0":
                trend3_customers = "No change"
                trend3_customers_percent = ""
            else:
                trend3_customers = "Up"
                trend3_customers_percent = "Infinity% (" + trend3_customers_diff + " customers) "
    except UnboundLocalError:
        trend3_customers = "N/A"
        trend3_customers_percent = "Data unavailable "

    try:
        trend6_customers_diff = customers - trend6_customers
        if trend6_customers_diff >= 0:
            trend6_customers_diff = "+" + str("{:,}".format(trend6_customers_diff))
        else:
            trend6_customers_diff = str("{:,}".format(trend6_customers_diff))

        try:
            trend6_customers_percent = ((customers - trend6_customers) / trend6_customers) * 100
            trend6_customers_percent = round(trend6_customers_percent, 2)
            if trend6_customers_percent < 0:
                trend6_customers = "Down"
                trend6_customers_percent = str(
                    "{:,}".format(abs(trend6_customers_percent))) + "% (" + trend6_customers_diff + " customers) "
            elif trend6_customers_percent == 0:
                trend6_customers = "No change"
                trend6_customers_percent = ""
            else:
                trend6_customers = "Up"
                trend6_customers_percent = str(
                    "{:,}".format(trend6_customers_percent)) + "% (" + trend6_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend6_customers_diff == "+0":
                trend6_customers = "No change"
                trend6_customers_percent = ""
            else:
                trend6_customers = "Up"
                trend6_customers_percent = "Infinity% (" + trend6_customers_diff + " customers) "
    except UnboundLocalError:
        trend6_customers = "N/A"
        trend6_customers_percent = "Data unavailable "

    try:
        trend12_customers_diff = customers - trend12_customers
        if trend12_customers_diff >= 0:
            trend12_customers_diff = "+" + str("{:,}".format(trend12_customers_diff))
        else:
            trend12_customers_diff = str("{:,}".format(trend12_customers_diff))
        try:
            trend12_customers_percent = ((customers - trend12_customers) / trend12_customers) * 100
            trend12_customers_percent = round(trend12_customers_percent, 2)
            if trend12_customers_percent < 0:
                trend12_customers = "Down"
                trend12_customers_percent = str(
                    "{:,}".format(abs(trend12_customers_percent))) + "% (" + trend12_customers_diff + " customers) "
            elif trend12_customers_percent == 0:
                trend12_customers = "No change"
                trend12_customers_percent = ""
            else:
                trend12_customers = "Up"
                trend12_customers_percent = str(
                    "{:,}".format(trend12_customers_percent)) + "% (" + trend12_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend12_customers_diff == "+0":
                trend12_customers = "No change"
                trend12_customers_percent = ""
            else:
                trend12_customers = "Up"
                trend12_customers_percent = "Infinity% (" + trend12_customers_diff + " customers) "
    except UnboundLocalError:
        trend12_customers = "N/A"
        trend12_customers_percent = "Data unavailable "

    try:
        trend1_outages_diff = outages - trend1_outages
        if trend1_outages_diff >= 0:
            trend1_outages_diff = "+" + str("{:,}".format(trend1_outages_diff))
        else:
            trend1_outages_diff = str("{:,}".format(trend1_outages_diff))

        try:
            trend1_outages_percent = ((outages - trend1_outages) / trend1_outages) * 100
            trend1_outages_percent = round(trend1_outages_percent, 2)
            if trend1_outages_percent < 0:
                trend1_outages = "Down"
                trend1_outages_percent = str(
                    "{:,}".format(abs(trend1_outages_percent))) + "% (" + trend1_outages_diff + " outages) "
            elif trend1_outages_percent == 0:
                trend1_outages = "No change"
                trend1_outages_percent = ""
            else:
                trend1_outages = "Up"
                trend1_outages_percent = str(
                    "{:,}".format(trend1_outages_percent)) + "% (" + trend1_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend1_outages_diff == "+0":
                trend1_outages = "No change"
                trend1_outages_percent = ""
            else:
                trend1_outages = "Up"
                trend1_outages_percent = "Infinity% (" + trend1_outages_diff + " outages) "
    except UnboundLocalError:
        trend1_outages = "N/A"
        trend1_outages_percent = "Data unavailable "

    try:
        trend3_outages_diff = outages - trend3_outages
        if trend3_outages_diff >= 0:
            trend3_outages_diff = "+" + str("{:,}".format(trend3_outages_diff))
        else:
            trend3_outages_diff = str("{:,}".format(trend3_outages_diff))

        try:
            trend3_outages_percent = ((outages - trend3_outages) / trend3_outages) * 100
            trend3_outages_percent = round(trend3_outages_percent, 2)
            if trend3_outages_percent < 0:
                trend3_outages = "Down"
                trend3_outages_percent = str(
                    "{:,}".format(abs(trend3_outages_percent))) + "% (" + trend3_outages_diff + " outages) "
            elif trend3_outages_percent == 0:
                trend3_outages = "No change"
                trend3_outages_percent = ""
            else:
                trend3_outages = "Up"
                trend3_outages_percent = str(
                    "{:,}".format(trend3_outages_percent)) + "% (" + trend3_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend3_outages_diff == "+0":
                trend3_outages = "No change"
                trend3_outages_percent = ""
            else:
                trend3_outages = "Up"
                trend3_outages_percent = "Infinity% (" + trend3_outages_diff + " outages) "
    except UnboundLocalError:
        trend3_outages = "N/A"
        trend3_outages_percent = "Data unavailable "

    try:
        trend6_outages_diff = outages - trend6_outages
        if trend6_outages_diff >= 0:
            trend6_outages_diff = "+" + str("{:,}".format(trend6_outages_diff))
        else:
            trend6_outages_diff = str("{:,}".format(trend6_outages_diff))

        try:
            trend6_outages_percent = ((outages - trend6_outages) / trend6_outages) * 100
            trend6_outages_percent = round(trend6_outages_percent, 2)
            if trend6_outages_percent < 0:
                trend6_outages = "Down"
                trend6_outages_percent = str(
                    "{:,}".format(abs(trend6_outages_percent))) + "% (" + trend6_outages_diff + " outages) "
            elif trend6_outages_percent == 0:
                trend6_outages = "No change"
                trend6_outages_percent = ""
            else:
                trend6_outages = "Up"
                trend6_outages_percent = str(
                    "{:,}".format(trend6_outages_percent)) + "% (" + trend6_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend6_outages_diff == "+0":
                trend6_outages = "No change"
                trend6_outages_percent = ""
            else:
                trend6_outages = "Up"
                trend6_outages_percent = "Infinity% (" + trend6_outages_diff + " outages) "
    except UnboundLocalError:
        trend6_outages = "N/A"
        trend6_outages_percent = "Data unavailable "

    try:
        trend12_outages_diff = outages - trend12_outages
        if trend12_outages_diff >= 0:
            trend12_outages_diff = "+" + str("{:,}".format(trend12_outages_diff))
        else:
            trend12_outages_diff = str("{:,}".format(trend12_outages_diff))

        try:
            trend12_outages_percent = ((outages - trend12_outages) / trend12_outages) * 100
            trend12_outages_percent = round(trend12_outages_percent, 2)
            if trend12_outages_percent < 0:
                trend12_outages = "Down"
                trend12_outages_percent = str(
                    "{:,}".format(abs(trend12_outages_percent))) + "% (" + trend12_outages_diff + " outages) "
            elif trend12_outages_percent == 0:
                trend12_outages = "No change"
                trend12_outages_percent = ""
            else:
                trend12_outages = "Up"
                trend12_outages_percent = str(
                    "{:,}".format(trend12_outages_percent)) + "% (" + trend12_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend12_outages_diff == "+0":
                trend12_outages = "No change"
                trend12_outages_percent = ""
            else:
                trend12_outages = "Up"
                trend12_outages_percent = "Infinity% (" + trend12_outages_diff + " outages) "
    except UnboundLocalError:
        trend12_outages = "N/A"
        trend12_outages_percent = "Data unavailable "

    return render_template('page.html', customers="{:,}".format(customers), outages="{:,}".format(outages), trend1_customers=trend1_customers,
                           percent1_customers=trend1_customers_percent, trend1_outages=trend1_outages,
                           percent1_outages=trend1_outages_percent, trend3_customers=trend3_customers,
                           percent3_customers=trend3_customers_percent, trend6_customers=trend6_customers,
                           percent6_customers=trend6_customers_percent, trend3_outages=trend3_outages,
                           percent3_outages=trend3_outages_percent, trend6_outages=trend6_outages,
                           percent6_outages=trend6_outages_percent, trend12_outages=trend12_outages, percent12_outages=trend12_outages_percent,
                           trend12_customers=trend12_customers, percent12_customers=trend12_customers_percent,
                           recordcustomer="{:,}".format(record_customers), lastupdated_time=last_updated_time, recordcustomer_time=record_customers_time,
                           recordoutage="{:,}".format(record_outages), recordoutage_time=record_outages_time, fulldata=fulldata)


@app.route("/historical/isaias", methods=['GET'])
def isaias():
    return render_template('isaias.html')


@app.route("/inboundVoice", methods=['POST'])
def inboundvoice():
    resp = VoiceResponse()
    say = Say(voice='Polly.Joanna')

    key = request.values.get('key', "None")
    if key != "idonthaveinternet":
        say.append('Bad API key.')
        resp.append(say)
        resp.hangup()
        response = Response(str(resp))
        return response

    tzdata = tz.gettz('America/New_York')
    year = str(datetime.datetime.now().year)
    month = str(datetime.datetime.now().month)
    day = str(datetime.datetime.now().day)
    hour = str(datetime.datetime.now().hour)
    minute = str(datetime.datetime.now().minute)
    second = "00"
    timezone = datetime.datetime.now(tz=tzdata).tzname()

    timezone_offset = "-0500"
    if timezone == "EDT":
        timezone_offset = "-0400"

    trend_time = datetime.datetime.now(tz=tzdata)
    trend_time_1h = trend_time + datetime.timedelta(hours=-1)
    trend_time_3h = trend_time + datetime.timedelta(hours=-3)
    trend_time_6h = trend_time + datetime.timedelta(hours=-6)
    trend_time_12h = trend_time + datetime.timedelta(hours=-12)

    trend1_year = str(trend_time_1h.year).zfill(2)
    trend1_month = str(trend_time_1h.month).zfill(2)
    trend1_day = str(trend_time_1h.day).zfill(2)
    trend1_hour = str(trend_time_1h.hour).zfill(2)
    trend1_minute = str(trend_time_1h.minute).zfill(2)
    trend1_second = "00"
    trend1_timezone = trend_time_1h.tzname()
    timezone_offset_trend1 = "-0500"
    if trend1_timezone == "EDT":
        timezone_offset_trend1 = "-0400"

    trend3_year = str(trend_time_3h.year).zfill(2)
    trend3_month = str(trend_time_3h.month).zfill(2)
    trend3_day = str(trend_time_3h.day).zfill(2)
    trend3_hour = str(trend_time_3h.hour).zfill(2)
    trend3_minute = str(trend_time_3h.minute).zfill(2)
    trend3_second = "00"
    trend3_timezone = trend_time_3h.tzname()
    timezone_offset_trend3 = "-0500"
    if trend3_timezone == "EDT":
        timezone_offset_trend3 = "-0400"

    trend6_year = str(trend_time_6h.year).zfill(2)
    trend6_month = str(trend_time_6h.month).zfill(2)
    trend6_day = str(trend_time_6h.day).zfill(2)
    trend6_hour = str(trend_time_6h.hour).zfill(2)
    trend6_minute = str(trend_time_6h.minute).zfill(2)
    trend6_second = "00"
    trend6_timezone = trend_time_6h.tzname()
    timezone_offset_trend6 = "-0500"
    if trend6_timezone == "EDT":
        timezone_offset_trend6 = "-0400"

    trend12_year = str(trend_time_12h.year).zfill(2)
    trend12_month = str(trend_time_12h.month).zfill(2)
    trend12_day = str(trend_time_12h.day).zfill(2)
    trend12_hour = str(trend_time_12h.hour).zfill(2)
    trend12_minute = str(trend_time_12h.minute).zfill(2)
    trend12_second = "00"
    trend12_timezone = trend_time_12h.tzname()
    timezone_offset_trend12 = "-0500"
    if trend12_timezone == "EDT":
        timezone_offset_trend12 = "-0400"

    year = year.zfill(2)
    month = month.zfill(2)
    day = day.zfill(2)
    hour = hour.zfill(2)
    minute = minute.zfill(2)
    minute = minute[0] + "0"

    trend1_minute = trend1_minute[0] + "0"
    trend3_minute = trend3_minute[0] + "0"
    trend6_minute = trend6_minute[0] + "0"
    trend12_minute = trend12_minute[0] + "0"

    formatted_date = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + " " + timezone_offset
    trend1_date = str(trend1_year) + "-" + str(trend1_month) + "-" + str(trend1_day) + " " + str(
        trend1_hour) + ":" + str(trend1_minute) + ":" + str(trend1_second) + " " + timezone_offset_trend1
    trend3_date = str(trend3_year) + "-" + str(trend3_month) + "-" + str(trend3_day) + " " + str(
        trend3_hour) + ":" + str(trend3_minute) + ":" + str(trend3_second) + " " + timezone_offset_trend3
    trend6_date = str(trend6_year) + "-" + str(trend6_month) + "-" + str(trend6_day) + " " + str(
        trend6_hour) + ":" + str(trend6_minute) + ":" + str(trend6_second) + " " + timezone_offset_trend6
    trend12_date = str(trend12_year) + "-" + str(trend12_month) + "-" + str(trend12_day) + " " + str(
        trend12_hour) + ":" + str(trend12_minute) + ":" + str(trend12_second) + " " + timezone_offset_trend12

    formatted_date_timestamp = datetime.datetime.strptime(formatted_date, "%Y-%m-%d %H:%M:%S %z")
    trend1_date_timestamp = datetime.datetime.strptime(trend1_date, "%Y-%m-%d %H:%M:%S %z")
    trend3_date_timestamp = datetime.datetime.strptime(trend3_date, "%Y-%m-%d %H:%M:%S %z")
    trend6_date_timestamp = datetime.datetime.strptime(trend6_date, "%Y-%m-%d %H:%M:%S %z")
    trend12_date_timestamp = datetime.datetime.strptime(trend12_date, "%Y-%m-%d %H:%M:%S %z")

    response = table.scan()
    fulldata = {}
    for i in response['Items']:
        timestamp = int(i['timestamp'])
        outagesthing = int(i['outages'])
        customersthing = int(i['customers'])
        fulldata[timestamp] = {
            "outages": outagesthing,
            "customers": customersthing
        }

    record_outages = -1
    record_customers = -1
    for i in response['Items']:
        if int(i['timestamp']) == int(formatted_date_timestamp.timestamp()):
            outages = int(i['outages'])
            customers = int(i['customers'])
            last_updated_time = formatted_date_timestamp.strftime("%B %-d at %l:%M %p")

        if int(i['outages']) > record_outages:
            record_outages = int(i['outages'])
            record_outages_time = datetime.datetime.fromtimestamp(int(i['timestamp'])).strftime("%B %-d at %l:%M %p")

        if int(i['customers']) > record_customers:
            record_customers = int(i['customers'])
            record_customers_time = datetime.datetime.fromtimestamp(int(i['timestamp'])).strftime("%B %-d at %l:%M %p")

        if int(i['timestamp']) == int(trend1_date_timestamp.timestamp()):
            trend1_outages = int(i['outages'])
            trend1_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend3_date_timestamp.timestamp()):
            trend3_outages = int(i['outages'])
            trend3_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend6_date_timestamp.timestamp()):
            trend6_outages = int(i['outages'])
            trend6_customers = int(i['customers'])

        if int(i['timestamp']) == int(trend12_date_timestamp.timestamp()):
            trend12_outages = int(i['outages'])
            trend12_customers = int(i['customers'])

    try:
        test_customers = customers
    except Exception:
        last_updated_time = formatted_date_timestamp + datetime.timedelta(minutes=-10)
        last_updated_time_key = int(last_updated_time.timestamp())
        last_updated_time = last_updated_time.strftime('%B %-d at %l:%M %p')
        customers = fulldata[int(last_updated_time_key)]['customers']
        outages = fulldata[int(last_updated_time_key)]['outages']

    try:
        trend1_customers_diff = customers - trend1_customers
        if trend1_customers_diff >= 0:
            trend1_customers_diff = "+" + str("{:,}".format(trend1_customers_diff))
        else:
            trend1_customers_diff = str("{:,}".format(trend1_customers_diff))

        try:
            trend1_customers_percent = ((customers - trend1_customers) / trend1_customers) * 100
            trend1_customers_percent = round(trend1_customers_percent, 2)
            if trend1_customers_percent < 0:
                trend1_customers = "Down"
                trend1_customers_percent = str(
                    "{:,}".format(abs(trend1_customers_percent))) + "% (" + trend1_customers_diff + " customers) "
            elif trend1_customers_percent == 0:
                trend1_customers = "No change"
                trend1_customers_percent = ""
            else:
                trend1_customers = "Up"
                trend1_customers_percent = str(
                    "{:,}".format(trend1_customers_percent)) + "% (" + trend1_customers_diff + " customers) "

        except ZeroDivisionError:
            if trend1_customers_diff == "+0":
                trend1_customers = "No change"
                trend1_customers_percent = ""
            else:
                trend1_customers = "Up"
                trend1_customers_percent = "Infinity% (" + trend1_customers_diff + " customers) "
    except UnboundLocalError:
        trend1_customers = "N/A"
        trend1_customers_percent = "Data unavailable "

    try:
        trend3_customers_diff = customers - trend3_customers
        if trend3_customers_diff >= 0:
            trend3_customers_diff = "+" + str("{:,}".format(trend3_customers_diff))
        else:
            trend3_customers_diff = str("{:,}".format(trend3_customers_diff))

        try:
            trend3_customers_percent = ((customers - trend3_customers) / trend3_customers) * 100
            trend3_customers_percent = round(trend3_customers_percent, 2)
            if trend3_customers_percent < 0:
                trend3_customers = "Down"
                trend3_customers_percent = str(
                    "{:,}".format(abs(trend3_customers_percent))) + "% (" + trend3_customers_diff + " customers) "
            elif trend3_customers_percent == 0:
                trend3_customers = "No change"
                trend3_customers_percent = ""
            else:
                trend3_customers = "Up"
                trend3_customers_percent = str(
                    "{:,}".format(trend3_customers_percent)) + "% (" + trend3_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend3_customers_diff == "+0":
                trend3_customers = "No change"
                trend3_customers_percent = ""
            else:
                trend3_customers = "Up"
                trend3_customers_percent = "Infinity% (" + trend3_customers_diff + " customers) "
    except UnboundLocalError:
        trend3_customers = "N/A"
        trend3_customers_percent = "Data unavailable "

    try:
        trend6_customers_diff = customers - trend6_customers
        if trend6_customers_diff >= 0:
            trend6_customers_diff = "+" + str("{:,}".format(trend6_customers_diff))
        else:
            trend6_customers_diff = str("{:,}".format(trend6_customers_diff))

        try:
            trend6_customers_percent = ((customers - trend6_customers) / trend6_customers) * 100
            trend6_customers_percent = round(trend6_customers_percent, 2)
            if trend6_customers_percent < 0:
                trend6_customers = "Down"
                trend6_customers_percent = str(
                    "{:,}".format(abs(trend6_customers_percent))) + "% (" + trend6_customers_diff + " customers) "
            elif trend6_customers_percent == 0:
                trend6_customers = "No change"
                trend6_customers_percent = ""
            else:
                trend6_customers = "Up"
                trend6_customers_percent = str(
                    "{:,}".format(trend6_customers_percent)) + "% (" + trend6_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend6_customers_diff == "+0":
                trend6_customers = "No change"
                trend6_customers_percent = ""
            else:
                trend6_customers = "Up"
                trend6_customers_percent = "Infinity% (" + trend6_customers_diff + " customers) "
    except UnboundLocalError:
        trend6_customers = "N/A"
        trend6_customers_percent = "Data unavailable "

    try:
        trend12_customers_diff = customers - trend12_customers
        if trend12_customers_diff >= 0:
            trend12_customers_diff = "+" + str("{:,}".format(trend12_customers_diff))
        else:
            trend12_customers_diff = str("{:,}".format(trend12_customers_diff))
        try:
            trend12_customers_percent = ((customers - trend12_customers) / trend12_customers) * 100
            trend12_customers_percent = round(trend12_customers_percent, 2)
            if trend12_customers_percent < 0:
                trend12_customers = "Down"
                trend12_customers_percent = str(
                    "{:,}".format(abs(trend12_customers_percent))) + "% (" + trend12_customers_diff + " customers) "
            elif trend12_customers_percent == 0:
                trend12_customers = "No change"
                trend12_customers_percent = ""
            else:
                trend12_customers = "Up"
                trend12_customers_percent = str(
                    "{:,}".format(trend12_customers_percent)) + "% (" + trend12_customers_diff + " customers) "
        except ZeroDivisionError:
            if trend12_customers_diff == "+0":
                trend12_customers = "No change"
                trend12_customers_percent = ""
            else:
                trend12_customers = "Up"
                trend12_customers_percent = "Infinity% (" + trend12_customers_diff + " customers) "
    except UnboundLocalError:
        trend12_customers = "N/A"
        trend12_customers_percent = "Data unavailable "

    try:
        trend1_outages_diff = outages - trend1_outages
        if trend1_outages_diff >= 0:
            trend1_outages_diff = "+" + str("{:,}".format(trend1_outages_diff))
        else:
            trend1_outages_diff = str("{:,}".format(trend1_outages_diff))

        try:
            trend1_outages_percent = ((outages - trend1_outages) / trend1_outages) * 100
            trend1_outages_percent = round(trend1_outages_percent, 2)
            if trend1_outages_percent < 0:
                trend1_outages = "Down"
                trend1_outages_percent = str(
                    "{:,}".format(abs(trend1_outages_percent))) + "% (" + trend1_outages_diff + " outages) "
            elif trend1_outages_percent == 0:
                trend1_outages = "No change"
                trend1_outages_percent = ""
            else:
                trend1_outages = "Up"
                trend1_outages_percent = str(
                    "{:,}".format(trend1_outages_percent)) + "% (" + trend1_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend1_outages_diff == "+0":
                trend1_outages = "No change"
                trend1_outages_percent = ""
            else:
                trend1_outages = "Up"
                trend1_outages_percent = "Infinity% (" + trend1_outages_diff + " outages) "
    except UnboundLocalError:
        trend1_outages = "N/A"
        trend1_outages_percent = "Data unavailable "

    try:
        trend3_outages_diff = outages - trend3_outages
        if trend3_outages_diff >= 0:
            trend3_outages_diff = "+" + str("{:,}".format(trend3_outages_diff))
        else:
            trend3_outages_diff = str("{:,}".format(trend3_outages_diff))

        try:
            trend3_outages_percent = ((outages - trend3_outages) / trend3_outages) * 100
            trend3_outages_percent = round(trend3_outages_percent, 2)
            if trend3_outages_percent < 0:
                trend3_outages = "Down"
                trend3_outages_percent = str(
                    "{:,}".format(abs(trend3_outages_percent))) + "% (" + trend3_outages_diff + " outages) "
            elif trend3_outages_percent == 0:
                trend3_outages = "No change"
                trend3_outages_percent = ""
            else:
                trend3_outages = "Up"
                trend3_outages_percent = str(
                    "{:,}".format(trend3_outages_percent)) + "% (" + trend3_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend3_outages_diff == "+0":
                trend3_outages = "No change"
                trend3_outages_percent = ""
            else:
                trend3_outages = "Up"
                trend3_outages_percent = "Infinity% (" + trend3_outages_diff + " outages) "
    except UnboundLocalError:
        trend3_outages = "N/A"
        trend3_outages_percent = "Data unavailable "

    try:
        trend6_outages_diff = outages - trend6_outages
        if trend6_outages_diff >= 0:
            trend6_outages_diff = "+" + str("{:,}".format(trend6_outages_diff))
        else:
            trend6_outages_diff = str("{:,}".format(trend6_outages_diff))

        try:
            trend6_outages_percent = ((outages - trend6_outages) / trend6_outages) * 100
            trend6_outages_percent = round(trend6_outages_percent, 2)
            if trend6_outages_percent < 0:
                trend6_outages = "Down"
                trend6_outages_percent = str(
                    "{:,}".format(abs(trend6_outages_percent))) + "% (" + trend6_outages_diff + " outages) "
            elif trend6_outages_percent == 0:
                trend6_outages = "No change"
                trend6_outages_percent = ""
            else:
                trend6_outages = "Up"
                trend6_outages_percent = str(
                    "{:,}".format(trend6_outages_percent)) + "% (" + trend6_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend6_outages_diff == "+0":
                trend6_outages = "No change"
                trend6_outages_percent = ""
            else:
                trend6_outages = "Up"
                trend6_outages_percent = "Infinity% (" + trend6_outages_diff + " outages) "
    except UnboundLocalError:
        trend6_outages = "N/A"
        trend6_outages_percent = "Data unavailable "

    try:
        trend12_outages_diff = outages - trend12_outages
        if trend12_outages_diff >= 0:
            trend12_outages_diff = "+" + str("{:,}".format(trend12_outages_diff))
        else:
            trend12_outages_diff = str("{:,}".format(trend12_outages_diff))

        try:
            trend12_outages_percent = ((outages - trend12_outages) / trend12_outages) * 100
            trend12_outages_percent = round(trend12_outages_percent, 2)
            if trend12_outages_percent < 0:
                trend12_outages = "Down"
                trend12_outages_percent = str(
                    "{:,}".format(abs(trend12_outages_percent))) + "% (" + trend12_outages_diff + " outages) "
            elif trend12_outages_percent == 0:
                trend12_outages = "No change"
                trend12_outages_percent = ""
            else:
                trend12_outages = "Up"
                trend12_outages_percent = str(
                    "{:,}".format(trend12_outages_percent)) + "% (" + trend12_outages_diff + " outages) "
        except ZeroDivisionError:
            if trend12_outages_diff == "+0":
                trend12_outages = "No change"
                trend12_outages_percent = ""
            else:
                trend12_outages = "Up"
                trend12_outages_percent = "Infinity% (" + trend12_outages_diff + " outages) "
    except UnboundLocalError:
        trend12_outages = "N/A"
        trend12_outages_percent = "Data unavailable "

    if customers < 100 and outages < 5:
        say.append("The CenHud Outage Trends call service is not available, as there are less than 100 customers without power, or less than 5 outages.")
        say.ssml_break(strength='x-strong', time='1s')
        say.append("Please visit c h trends dot owen the dot dev for outage trend information. Goodbye!")
        resp.append(say)
        resp.hangup()
        response = Response(str(resp))
        response.headers.add('Access-Control-Allow-Origin', "*")
        return response

    say.append("Last updated: " + last_updated_time + " Eastern Time")
    say.ssml_break(strength='x-strong', time='1s')
    say.append("Right now, there are " + "{:,}".format(customers) + " customers out, and " + "{:,}".format(outages) + " outages.")
    say.ssml_break(strength='x-strong', time='1s')
    if customers == record_customers:
        say.append("The number of customers out is a new record for the past 72 hours.")
        say.ssml_break(strength='x-strong', time='1s')

    if outages == record_outages:
        say.append("The number of outages is a new record for the past 72 hours.")
        say.ssml_break(strength='x-strong', time='1s')

    if trend1_customers == "No change":
        say.append("In the last hour, customers out have not changed.")
    elif trend1_customers == "N/A":
        say.append("Customers out trend data is not available for the last hour.")
    else:
        say.append("In the last hour, customers out are " + trend1_customers + " by " + trend1_customers_percent)
    say.ssml_break(strength='x-strong', time='1s')

    if trend1_outages == "No change":
        say.append("In the last hour, outages have not changed.")
    elif trend1_outages == "N/A":
        say.append("Outages trend data is not available for the last hour.")
    else:
        say.append("In the last hour, outages are " + trend1_outages + " by " + trend1_outages_percent)
    say.ssml_break(strength='x-strong', time='1s')

    if trend3_customers == "No change":
        say.append("In the last three hours, customers out have not changed.")
    elif trend3_customers == "N/A":
        say.append("Customers out trend data is not available for the last three hours.")
    else:
        say.append("In the last three hours, customers out are " + trend3_customers + " by " + trend3_customers_percent)
    say.ssml_break(strength='x-strong', time='1s')

    if trend3_outages == "No change":
        say.append("In the last three hours, outages have not changed.")
    elif trend3_outages == "N/A":
        say.append("Outages trend data is not available for the last three hours.")
    else:
        say.append("In the last three hours, outages are " + trend3_outages + " by " + trend3_outages_percent)
    say.ssml_break(strength='x-strong', time='1s')

    if trend6_customers == "No change":
        say.append("In the last six hours, customers out have not changed.")
    elif trend6_customers == "N/A":
        say.append("Customers out trend data is not available for the last six hours.")
    else:
        say.append("In the last six hours, customers out are " + trend6_customers + " by " + trend6_customers_percent)
    say.ssml_break(strength='x-strong', time='1s')

    if trend6_outages == "No change":
        say.append("In the last six hours, outages have not changed.")
    elif trend6_outages == "N/A":
        say.append("Outages trend data is not available for the last six hours.")
    else:
        say.append("In the last six hours, outages are " + trend6_outages + " by " + trend6_outages_percent)
    say.ssml_break(strength='x-strong', time='1s')

    # Commented out code is for repeating back the record number, but this may be changed.
    #say.append("The record number of customers out is " + "{:,}".format(record_customers) + ", recorded at " + record_customers_time + ".")
    #say.ssml_break(strength='x-strong', time='1s')
    #say.append("The record number of outages is " + "{:,}".format(
    #    record_outages) + ", recorded at " + record_outages_time + ".")
    #say.ssml_break(strength='x-strong', time='1s')
    say.append("For more information, please visit c h trends dot owen the dot dev.")
    say.ssml_break(strength='x-strong', time='1s')
    say.append("Thank you for using the Central Hudson Outage Trends call service. Goodbye!")
    resp.append(say)
    resp.hangup()
    response = Response(str(resp))
    response.headers.add('Access-Control-Allow-Origin', "*")
    return response

if __name__ == "__main__":
    app.run()