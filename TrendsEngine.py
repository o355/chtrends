from JSONWrapper import JSONWrapper
from datetime import datetime

# CenHud Outage Trends (chtrends) Version 3.1.1
# Copyright (C) 2021  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class TrendsEngine(JSONWrapper):
    """
    TrendsEngine is the highest-level abstraction for the outage data. It directly interfaces with the web interface.
    """

    def __init__(self, configfile, autopopulate=True):
        """
        Constructs a new TrendsEngine object.
        :param configfile: The configuration file to read from.
        :param autopopulate: Whether to automatically populate the TrendsEngine object with data on startup. This will cause
        one scan of the database. Otherwise, you'll need to call updatedata() before grabbing any trend data.
        """
        super().__init__(configfile)
        self.data = None
        self.avgdata = None
        self.latestkey = None
        if autopopulate:
            self.updatedata()

    def updatedata(self):
        self.data = self.getall()
        self.avgdata = self.__centeraverage(self.data)
        self.latestkey = self.__determinelatestkey(self.data)

    def __determinelatestkey(self, data):
        """
        Determines the latest key from the data.
        :param data: A DynamoDBWrapper .getall() data dictionary.
        :return: A UNIX timestamp key (as string) that represents the most recent data in the database.
        """

        return list(data.keys())[-1]

    def __centeraverage(self, data):
        """
        Hard coded private method to return a dictionary of 3-point centered averages.
        :param data: A DyanmoDBWrapper .getall() data dictionary.
        :return: A dictionary containing a 3-point centered average of any applicable points.
        """

        centered = {}
        for key in data.keys():
            try:
                centeredpoint_outages = (data[str(int(key) - 600)]['outages'] + data[key]['outages'] + data[str(int(key) + 600)]['outages']) / 3
                centeredpoint_customers = (data[str(int(key) - 600)]['customers'] + data[key]['customers'] + data[str(int(key) + 600)]['customers']) / 3
            except KeyError:
                continue

            centered[key] = {"customers": centeredpoint_customers, "outages": centeredpoint_outages}

        return centered

    def getlatest(self):
        if self.data is None:
            raise RuntimeError("Please run updatedata() before generating trends.")

        returndata = {"timestamp": int(self.latestkey),
                      "formattedtime": self.__formattime(self.latestkey),
                      "customers": self.data[self.latestkey]['customers'],
                      "customers_formatted": str("{:,}".format(self.data[self.latestkey]['customers'])),
                      "outages": self.data[self.latestkey]['outages'],
                      "outages_formatted": str("{:,}".format(self.data[self.latestkey]['outages']))}

        return returndata

    def __formattime(self, timestamp):
        return datetime.strftime(datetime.fromtimestamp(int(timestamp)), "%B %-d, %Y at %l:%M %p").replace("  ", " ")

    # TODO - Generate Trends
    def gettrends(self, intervals, datatype, usecentered, useminutes=False):
        """
        Generates trend data.
        :param intervals: An array of interval hours for which to generate data at.
        :param datatype: Which datatype to use. Either customers or outages.
        :param usecentered: Whether to use the centered average array (true), or to use the raw data (false).
        :param useminutes: Whether you are passing your intervals in as minutes.
        :return: An array with dictionaries for each
        """

        # Enforce int for v3.1.0 of API.

        if self.data is None:
            raise RuntimeError("Please run updatedata() before generating trends.")

        if datatype != "customers" and datatype != "outages":
            raise RuntimeError("Supported datatypes are customers or outages.")

        datareturn = []
        for interval in intervals:
            try:
                test = int(interval)
            except TypeError:
                raise RuntimeError("Interval must be an integer.")

            if useminutes:
                if interval % 10 != 0:
                    raise RuntimeError("Intervals as minutes must be divisible by 10.")

            english_interval = ""
            english_range = ""
            interval_minutes = 0
            interval_hours = 0
            if useminutes:
                interval_hours = interval // 60
                interval_minutes = interval % 60
            else:
                interval_hours = interval

            english_interval = ""
            english_range = "last "

            if interval_hours == 1:
                english_interval = "1 hour"
                if useminutes:
                    english_range = "last 1 hour"
                else:
                    english_range = "last hour"
            elif interval_hours > 0:
                english_interval = str(interval_hours) + " hours"
                english_range = english_range + str(interval_hours) + " hours"

            if useminutes:
                comma = ""
                if interval_hours >= 1:
                    comma = ", "

                if interval_minutes == 1:
                    english_interval += comma + "1 minute"
                    english_range += comma + "1 minute"
                elif interval_minutes >= 1:
                    english_interval += comma + str(interval_minutes) + " minutes"
                    english_range += comma + str(interval_minutes) + " minutes"


            try:
                if usecentered:
                    if useminutes:
                        datapoint = self.avgdata[str(int(self.latestkey) - (interval * 60))]
                    else:
                        datapoint = self.avgdata[str(int(self.latestkey) - (interval * 3600))]
                else:
                    if useminutes:
                        datapoint = self.data[str(int(self.latestkey) - (interval * 60))]
                    else:
                        datapoint = self.data[str(int(self.latestkey) - (interval * 3600))]
            except KeyError:
                datareturn.append({"duration": english_interval, "change_direction": "N/A",
                                   "change_english": "Data unavailable in the " + english_range})
                continue

            datapoint_latest = self.data[self.latestkey][datatype]
            datapoint_comparison = datapoint[datatype]
            datapoint_difference = int(datapoint_latest - datapoint_comparison)
            datapoint_difference_amount = str("{:,}".format(datapoint_difference))

            try:
                trend_percent = round(((datapoint_latest - datapoint_comparison) / datapoint_comparison) * 100, 2)
                datapoint_difference_percent = str("{:,}".format(abs(trend_percent)))
            except ZeroDivisionError:
                if datapoint_difference == 0:
                    datareturn.append({"duration": english_interval, "change_percent": 0, "change_numeric": 0,
                                       "change_direction": "No change", "change_english": "in the " + english_range})
                else:
                    datareturn.append({"duration": english_interval, "change_percent": "infinity",
                                       "change_numeric": datapoint_difference, "change_direction": "Up",
                                       "change_english": "Infinity% (" + datapoint_difference_amount + " " + datatype + ") in the " + english_range})

                continue

            datapoint_dict = {"duration": english_interval, "change_percent": trend_percent,
                              "change_numeric": datapoint_difference}
            if trend_percent < 0:
                datapoint_dict['change_direction'] = "Down"
                datapoint_dict["change_english"] = datapoint_difference_percent + "% (" + datapoint_difference_amount + " " + datatype + ") in the " + english_range
            elif trend_percent == 0:
                datapoint_dict['change_direction'] = "No change"
                datapoint_dict['change_english'] = "in the " + english_range
            elif trend_percent > 0:
                datapoint_dict['change_direction'] = "Up"
                datapoint_dict["change_english"] = datapoint_difference_percent + "% (+" + datapoint_difference_amount + " " + datatype + ") in the " + english_range

            datareturn.append(datapoint_dict)

        return datareturn

    def getrecords(self):
        """
        Gets the datapoints with the highest number of customers out and outages, with timestamps.
        :return: A dictionary containing the record number of customers out and outages.
        """
        if self.data is None:
            raise RuntimeError("Please run updatedata() before generating trends.")

        top_customers = -1
        top_customers_time = -1
        top_customers_newrecord = False

        top_outages = -1
        top_outages_time = -1
        top_outages_newrecord = False

        for key in self.data.keys():
            if self.data[key]["customers"] >= top_customers:
                top_customers = self.data[key]["customers"]
                top_customers_time = int(key)

            if self.data[key]["outages"] >= top_outages:
                top_outages = self.data[key]["outages"]
                top_outages_time = int(key)

        if top_customers == -1 or top_outages == -1:
            raise RuntimeError("Records were unable to be calculated. Ensure there's at least 1 data point available.")

        top_customers_newrecord = top_customers_time == int(self.latestkey)
        top_outages_newrecord = top_outages_time == int(self.latestkey)

        return {"customers": {
                    "number": top_customers,
                    "number_formatted": str("{:,}".format(top_customers)),
                    "timestamp": top_customers_time,
                    "formatted_time": self.__formattime(top_customers_time),
                    "newrecord": top_customers_newrecord
                },
                "outages": {
                    "number": top_outages,
                    "number_formatted": str("{:,}".format(top_outages)),
                    "timestamp": top_outages_time,
                    "formatted_time": self.__formattime(top_outages_time),
                    "newrecord": top_outages_newrecord
                }
            }