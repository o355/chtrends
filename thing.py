# CenHud Outage Trends (chtrends) Version 2.0.4
# Copyright (C) 2020  Owen (owenthe.dev)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
import boto3
import datetime
import json
import time
import sys
from dateutil import tz
import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger("CHTrends")
logger.setLevel(logging.DEBUG)
logging.basicConfig(format='%(asctime)s %(message)s')
logger.propagate = False

# Turn off logging
#handler = RotatingFileHandler("/root/chtrends.log", backupCount=30)
#handler.setLevel(logging.DEBUG)
#handler.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
#logger.addHandler(handler)
#handler.doRollover()

access_key_aws = "ENTER_YOUR_ACCESS_KEY_HERE"
secret_key_aws = "ENTER_YOUR_SECRET_KEY_HERE"

logger.info("Starting CH Trends data get.")

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name='us-east-2')
db = session.resource('dynamodb')
table = db.Table('PowerOutages_Unix')

headers = {
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Content-Type': 'application/x-www-form-urlencoded;charset=ISO-8859-1',
    'DNT': '1',
    'Host': 's3.amazonaws.com',
    'Referer': 'https://s3.amazonaws.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0',
    'X-Requested-With': 'XMLHttpRequest'
}

headers_chtrendsbot = {
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Content-Type': 'application/x-www-form-urlencoded;charset=ISO-8859-1',
    'DNT': '1',
    'Host': 's3.amazonaws.com',
    'Referer': 'https://s3.amazonaws.com',
    'User-Agent': 'chtrends/2.0.5'
}

tzinfo = tz.gettz('America/New_York')

year = str(datetime.datetime.utcnow().year)
month = str(datetime.datetime.utcnow().month)
day = str(datetime.datetime.utcnow().day)
hour = str(datetime.datetime.utcnow().hour)
minute = str(datetime.datetime.utcnow().minute)
second = "00"

year = year.zfill(2)
month = month.zfill(2)
day = day.zfill(2)
hour = hour.zfill(2)
minute = minute.zfill(2)

time_now = datetime.datetime.now(tz=tzinfo)
oneday_time = time_now + datetime.timedelta(days=-3, minutes=-10)

# Fix for Python 3.8: %Z -> %z
formatted_time = datetime.datetime.now(tz=tzinfo).strftime("%Y-%m-%d %R:00 %z")
oneday_formatted_time = oneday_time.strftime("%Y-%m-%d %R:00 %z")

# Unix timestamp compatibility layer
formatted_time_timestamp = datetime.datetime.strptime(formatted_time, "%Y-%m-%d %H:%M:%S %z")
oneday_formatted_timestamp = datetime.datetime.strptime(oneday_formatted_time, "%Y-%m-%d %H:%M:%S %z")

scan = table.scan()
curtime = (time.time() // 60 * 60)
purgestamp = int((time.time() // 60 * 60) - 259200)
print(purgestamp)
logger.info("Trimming old data from database...")
for i in scan['Items']:
    timestamp = int(i['timestamp'])
    print(timestamp)
    if timestamp < purgestamp:
        table.delete_item(
            Key={
                'timestamp': timestamp
            }
        )

logger.info("Waiting 30 seconds for data to become available...")
time.sleep(30)

logger.info("Getting dictionary to use...")
dicttouse = ""
try:
    logger.info("Request URL: https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/metadata.json")
    site = requests.get("https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/metadata.json", headers=headers_chtrendsbot)
    logger.info(site)
    logger.info("Raw response from dictionary to use: %s" % site.text)
    data = json.loads(site.text)
    dicttouse = data['directory']
    logger.info("The dict we are using is %s" % dicttouse)
except:
    logger.info("Failed to parse the dictionary to use. Assuming use of dictionary.")
    dicttouse = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_00"
    logger.info("Using the dictionary: %s" % dicttouse)

try:
    logger.info("Request URL: https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/%s/data.json" % dicttouse)
    site = requests.get("https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/%s/data.json" % dicttouse, headers=headers_chtrendsbot)
    logger.info(site)
    logger.info("Raw response from website: %s" % site.text)
    data = json.loads(site.text)
    logger.info("Loaded JSON data")
    outages = data['summaryFileData']['total_outages']
    customers = data['summaryFileData']['total_cust_a']['val']
    logger.info("Outages: %s, Customers: %s" % (str(outages), str(customers)))
except:
    logger.info("Failed to properly parse data. Trying again with real browser UA in 30 seconds...")
    time.sleep(30)
    try:
        logger.info(
            "Request URL: https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/%s/data.json" % dicttouse)
        site = requests.get(
            "https://s3.amazonaws.com/stormcentral.cenhud.com/resources/data/external/interval_generation_data/%s/data.json" % dicttouse, headers=headers)
        logger.info(site)
        logger.info("Raw response from website: %s" % site.text)
        data = json.loads(site.text)
        logger.info("Loaded JSON data")
        outages = data['summaryFileData']['total_outages']
        customers = data['summaryFileData']['total_cust_a']['val']
        logger.info("Outages: %s, Customers: %s" % (str(outages), str(customers)))
    except:
        logger.info("Failed to re-request data. Script now exiting.")
        sys.exit()

logger.info("Putting item in table...")
table.put_item(
    Item={
        'timestamp': int(curtime),
        'outages': outages,
        'customers': customers
    }
)
logger.info("Script complete.")