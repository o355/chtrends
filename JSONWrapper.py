import configparser
import json


class JSONWrapper:
    """
    This class is a data wrapper for a local JSON file.
    """

    def __init__(self, configfile):
        """
        Construct a new JSONWrapper object.
        :param configfile: A configuration file to read from to access the JSON filename.
        """

        self.config = configparser.ConfigParser()
        self.config.read(configfile)

        self.filename = self.config['JSON']['filename']

    def __getjson(self):
        """
        Gets JSON data as read from the file.
        :return: A dictionary with the contents of the JSON file.
        """
        with open(self.filename) as file:
            return json.load(file)

    def __dumpjson(self, data):
        """
        Dumps a dictionary to the JSON file.
        :param data: A dictionary to dump to the JSON file.
        :return: Nothing
        """
        with open(self.filename, "w") as file:
            json.dump(data, file)

        return

    def getall(self):
        """
        Does a whole table scan, returning all data that CHTrends knows of in the DB.
        :return: A Dictionary of format: timestamp as string for the key, customers/outages for each timestamp key as integers.
        """
        scan = self.__getjson()

        sorted_returnobj = {}
        for key in sorted(scan):
            sorted_returnobj[key] = scan[key]

        return sorted_returnobj

    def put(self, timestamp, outages, customers):
        """
        Puts new data in the database.
        :param timestamp: The UNIX timestamp to associate this key with. Must be convertible to an integer.
        :param outages: The number of outages that occurred at this timestamp. Must be convertible to an integer.
        :param customers: THe number of customers out that occurred at this timestamp. Must be convertible to an integer.
        :return: TypeError on bad conversion, Python error if there's an issue dumping to the JSON, otherwise nothing if successful.
        """
        try:
            int(timestamp)
            int(outages)
            int(customers)
        except TypeError:
            raise TypeError("Timestamp, outages, and/or customers failed conversion to an integer.")

        data = self.__getjson()
        data[str(timestamp)] = {"customers": int(customers), "outages": int(outages)}
        self.__dumpjson(data)

    def delete(self, timestamp):
        """
        Deletes a data point from the database. This will NOT do retroactive deletion - please use the CHData class to handle this.
        :param timestamp: The timestamp of the data point to delete. Must be convertible to an integer.
        :return: TypeError on bad conversion, Python error if there's an issue dumping to the JSON, otherwise nothing if successful.
        """

        try:
            int(timestamp)
        except TypeError:
            raise TypeError("Timestamp failed conversion to an integer.")

        data = self.__getjson()
        data.pop(str(timestamp))

        self.__dumpjson(data)
